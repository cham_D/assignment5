#include <stdio.h>

int attendance(int price){
	int t,change,ch_at;
	t=price;
	int x;	//x; attendence
	change=price-15;
	/* the change of price of a ticket by 1 rupee, makes a change of 4 in the attendance
	 * the change of price is inversely proportional to the number of attendees */
	ch_at=change*4;//ch_at; change of attendance
	if (ch_at<0){
		x=-1*ch_at+120;
	}
	if (ch_at>0){
		x=120-ch_at;
	}
	if (ch_at==0){
		x=120;
	}
	return x;

}
	
int profit(int price){
	int t2;
	t2=price;
	int p;//p; profit
	/*cost per perfomance= 500 + attendees x 3
	 * profit= price of a ticket x attendees - cost */
	p=attendance(price)*t2-3*attendance(price)-500;
	return p;
}

int main(){
	int price;
	printf("Enter a ticket price: ");
	scanf("%d", &price);
	printf("Estimated number of attendees: %d \n", attendance(price));
	printf("Estimated profit: %d \n", profit(price));
	return 0;
}



